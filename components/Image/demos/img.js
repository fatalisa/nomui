define([], function () {
  return {
    title: '图片',
    file: 'img',
    demo: function () {
      return {
        component: 'Image',
        src: 'https://pic1.zhimg.com/80/v2-c589a8fd036cf547de7fc79dc05e6109_r.jpg',
      }
    },
  }
})
