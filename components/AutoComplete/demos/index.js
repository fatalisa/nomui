define(['./basic.js', './debounce.js', './filter.js', './remote.js'], function () {
  return {
    title: 'AutoComplete',
    subtitle: '自动完成',
    demos: Array.from(arguments),
  }
})
